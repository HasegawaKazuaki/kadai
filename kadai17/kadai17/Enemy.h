#include "Character.h"
class Enemy :public Character
{
public:
	Enemy();
	void DispHP();
	int Attack(int i);
	void Damage(int i);
};