#pragma once
#include <iostream>

class Character
{
protected:
	int hp, atk, def;
	int damage = 0;

public:
	void DispHP();
	int Attack(int i);//引数:相手の防御力
	void Damage(int i);//引数:受けるダメージ量
	int GetDef();
	bool IsDead();


};