#include "Character.h"


//HP表示
void Character::DispHP()
{
	std::cout << "プレイヤーHP=" << hp << "\n";
}

//攻撃
int Character::Attack(int i)
{
	std::cout << ("プレイヤーの攻撃\n");
	damage = atk - i / 2;
	return  damage;
}

//ダメージを受ける
void Character::Damage(int i)
{
	hp = hp - i;
	std::cout << "プレイヤーは" << i << "のダメージ\n";
}

//防御力を取得
int Character::GetDef()
{
	return def;
}

//戦闘不能判定
bool Character::IsDead()
{
	if (hp <= 0)
		return true;

	return false;

}
