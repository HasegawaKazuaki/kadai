#pragma once
class Player
{
	int hp, atk, def;
	int damage = 0;

public:
	Player();
	void DispHP();
	int Attack(int i);
	void Damage(int i);
	int GetDef();
	bool IsDead();
};